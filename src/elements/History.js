import history_icon from "../assets/historia_ikona.svg";

const History = ({ chosenCategory, setChosenCategory }) => {
  return (
    <li
      onClick={() => {
        setChosenCategory("history");
      }}
      className={chosenCategory + "Small"}
    >
      <img src={history_icon} alt="history" />
      <hr />
      <p>Historia</p>
    </li>
  );
};

export default History;
