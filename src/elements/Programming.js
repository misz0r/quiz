import programming_icon from "../assets/programowanie_ikona.svg";

const Programming = ({ chosenCategory, setChosenCategory }) => {
  return (
    <li
      onClick={() => {
        setChosenCategory("programming");
      }}
      className={chosenCategory + "Small"}
    >
      <img src={programming_icon} alt="programming" />
      <hr />
      <p>Programowanie</p>
    </li>
  );
};

export default Programming;
