import technology_icon from "../assets/technologia_ikona_.svg";

const Technology = ({ chosenCategory, setChosenCategory }) => {
  return (
    <li
      onClick={() => {
        setChosenCategory("technology");
      }}
      className={chosenCategory + "Small"}
    >
      <img src={technology_icon} alt="technology" />
      <hr />
      <p>Technologia</p>
    </li>
  );
};

export default Technology;
