import { Droppable } from "react-beautiful-dnd";
import { useEffect, useState } from "react";

import AnswerDND from "../elements/AnswerDND";

const Column = ({
  initialData,
  chosenCategory,
  questionNumber,
  setQuestionNumber,
  addStyle,
  chosenAnswer,
}) => {
  const [width, setWidth] = useState(window.innerWidth);
  const result = initialData[chosenCategory][questionNumber];

  useEffect(() => {
    function handleResize() {
      setWidth(window.innerWidth);
    }
    window.addEventListener("resize", handleResize);
  }, []);

  return (
    <div>
      <Droppable
        droppableId={result.columns["column-1"].id}
        direction={width > 992 ? "horizontal" : "vertical"}
      >
        {(provided) => (
          <div
            className="answers"
            ref={provided.innerRef}
            {...provided.droppableProps}
          >
            {result.columns["column-1"].choicesIds.map((choice, index) => (
              <AnswerDND
                key={choice}
                choice={choice}
                index={index}
                chosenCategory={chosenCategory}
                setQuestionNumber={setQuestionNumber}
                chosenAnswer={chosenAnswer}
                initialData={initialData}
                questionNumber={questionNumber}
                addStyle={addStyle}
              />
            ))}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </div>
  );
};

export default Column;
