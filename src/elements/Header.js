import backBtn from "../assets/cofnij_x.svg";
import exitBtn from "../assets/zamknij_x.svg";

const Header = () => {
  return (
    <div className="header">
      <img src={backBtn} alt="back" />
      <h1>Quiz</h1>
      <img src={exitBtn} alt="exit" />
    </div>
  );
};

export default Header;
