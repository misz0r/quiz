import { useState } from "react";

import Categories from "../components/categories";
import ConfirmCategoryPage from "./confirmCategoryPage";

import q from "../assets/logo_q.png";

const Homepage = () => {
  const [chosenCategory, setChosenCategory] = useState("");

  return (
    <div className="homepage">
      {chosenCategory.length > 2 ? (
        <ConfirmCategoryPage
          chosenCategory={chosenCategory}
          setChosenCategory={setChosenCategory}
        />
      ) : (
        <>
          <div className="header">
            <img src={q} alt="Q" className="q" />
            <h1 style={{ width: "100%", textAlign: "center" }}>Quiz</h1>
          </div>
          <Categories setChosenCategory={setChosenCategory} />
        </>
      )}
    </div>
  );
};

export default Homepage;
