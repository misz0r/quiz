import Culture from "../elements/Culture";
import History from "../elements/History";
import Moto from "../elements/Moto";
import Programming from "../elements/Programming";
import Technology from "../elements/Technology";
import QuestionPage from "./questionPage";

import NewTest from "../components/newTest";

const SummaryPage = ({
  chosenCategory,
  setChosenCategory,
  confirmedCategory,
  setConfirmedCategory,
  setQuestionNumber,
  correctAnswers,
  setCorrectAnswers,
  setImgSrc,
  setScoreSrc,
}) => {
  const showRightDescription = () => {
    switch (chosenCategory) {
      case "technology":
        return <Technology />;
      case "moto":
        return <Moto />;
      case "history":
        return <History />;
      case "culture":
        return <Culture />;
      case "programming":
        return <Programming />;
      default:
        return;
    }
  };

  const resetClick = () => {
    setQuestionNumber(1);
    setCorrectAnswers(0);
  };

  return (
    <>
      {confirmedCategory ? (
        <div className="summarison">
          <div className="summary">
            {showRightDescription()}
            <div className="score-wrapper">
              <img src={setScoreSrc()} alt="tryAgain" />
              <p className={"bar_" + chosenCategory + " score-description"}>
                Twój wynik<span>{correctAnswers}/10</span>
              </p>
            </div>
            <label className="tryAgain">
              <input
                className={"button_" + chosenCategory}
                type="submit"
                value="powtórz quiz"
                onClick={() => resetClick()}
              />
              <img src={setImgSrc()} alt="start" />
            </label>
          </div>
          <NewTest
            chosenCategory={chosenCategory}
            setChosenCategory={setChosenCategory}
            setConfirmedCategory={setConfirmedCategory}
          />
        </div>
      ) : (
        <QuestionPage />
      )}
    </>
  );
};

export default SummaryPage;
