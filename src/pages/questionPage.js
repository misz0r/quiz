import { useState } from "react";

import Answer from "../elements/Answer";
import SummaryPage from "./summaryPage";
import Question from "../elements/Question";

import cultureScore from "../assets/culture_score.png";
import historyScore from "../assets/history_score.png";
import motoScore from "../assets/moto_score.png";
import programmingScore from "../assets/programming_score.png";
import technologyScore from "../assets/programowanie_pole_pytania_.svg";

const QuestionPage = ({
  chosenCategory,
  setChosenCategory,
  confirmedCategory,
  setConfirmedCategory,
  setImgSrc,
}) => {
  const [questionNumber, setQuestionNumber] = useState(1);
  const [correctAnswers, setCorrectAnswers] = useState(0);

  const setScoreSrc = () => {
    switch (chosenCategory) {
      case "technology":
        return technologyScore;
      case "moto":
        return motoScore;
      case "history":
        return historyScore;
      case "culture":
        return cultureScore;
      case "programming":
        return programmingScore;
      default:
        return;
    }
  };

  return (
    <>
      {questionNumber > 10 ? (
        <SummaryPage
          chosenCategory={chosenCategory}
          setChosenCategory={setChosenCategory}
          confirmedCategory={confirmedCategory}
          setConfirmedCategory={setConfirmedCategory}
          setQuestionNumber={setQuestionNumber}
          correctAnswers={correctAnswers}
          setCorrectAnswers={setCorrectAnswers}
          setImgSrc={setImgSrc}
          setScoreSrc={setScoreSrc}
        />
      ) : (
        <>
          <Question
            chosenCategory={chosenCategory}
            questionNumber={questionNumber}
            setScoreSrc={setScoreSrc}
          />
          <Answer
            chosenCategory={chosenCategory}
            questionNumber={questionNumber}
            setQuestionNumber={setQuestionNumber}
            setCorrectAnswers={setCorrectAnswers}
          />
        </>
      )}
    </>
  );
};

export default QuestionPage;
