import Culture from "../elements/Culture";
import History from "../elements/History";
import Moto from "../elements/Moto";
import Programming from "../elements/Programming";
import Technology from "../elements/Technology";

const Categories = ({ setChosenCategory }) => {
  return (
    <>
      <p className="categoryTitle">10 pytań / 5 kategorii</p>
      <h3>Wybierz kategorię</h3>
      <ul className="categories">
        <Technology setChosenCategory={setChosenCategory} />
        <Culture setChosenCategory={setChosenCategory} />
        <Moto setChosenCategory={setChosenCategory} />
        <Programming setChosenCategory={setChosenCategory} />
        <History setChosenCategory={setChosenCategory} />
      </ul>
    </>
  );
};

export default Categories;
