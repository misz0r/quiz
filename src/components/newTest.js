import Culture from "../elements/Culture";
import History from "../elements/History";
import Moto from "../elements/Moto";
import Programming from "../elements/Programming";
import Technology from "../elements/Technology";

const NewTest = ({
  chosenCategory,
  setChosenCategory,
  setConfirmedCategory,
}) => {
  const showOtherCategories = () => {
    if (chosenCategory === "technology") {
      return (
        <>
          <Programming
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <Culture
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <Moto
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <History
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
        </>
      );
    } else if (chosenCategory === "moto") {
      return (
        <>
          <Programming
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <Culture
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <History
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <Technology
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
        </>
      );
    } else if (chosenCategory === "history") {
      return (
        <>
          <Programming
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <Culture
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <Technology
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <Moto
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
        </>
      );
    } else if (chosenCategory === "culture") {
      return (
        <>
          <Programming
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <Technology
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <Moto
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <History
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
        </>
      );
    } else if (chosenCategory === "programming") {
      return (
        <>
          <Technology
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <Moto
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <History
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
          <Culture
            setChosenCategory={setChosenCategory}
            chosenCategory={chosenCategory}
          />
        </>
      );
    }
  };

  return (
    <div className="otherCategoriesWrapper">
      <p>Zrób inny test</p>
      <ul
        className="otherCategories"
        onClick={() => setConfirmedCategory((prevState) => !prevState)}
      >
        {showOtherCategories()}
      </ul>
    </div>
  );
};

export default NewTest;
